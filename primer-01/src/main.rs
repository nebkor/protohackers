use std::{
    io::{Read, Result, Write},
    net::{Shutdown, TcpListener, TcpStream},
    sync::{atomic::AtomicU8, atomic::Ordering::SeqCst, Arc},
    time::Duration,
};

use rayon::ThreadPoolBuilder;

type Counter = Arc<AtomicU8>;

const ADDR: (&str, u16) = ("0.0.0.0", 6969);
const NUM_THREADS: usize = 10;
const MAX_CONNS: usize = NUM_THREADS * 10;

fn main() {
    let listener = TcpListener::bind(ADDR).unwrap();

    let pool = ThreadPoolBuilder::new()
        .num_threads(NUM_THREADS)
        .build()
        .unwrap();

    let conns = Arc::new(AtomicU8::new(0));

    for stream in listener.incoming() {
        if (conns.load(SeqCst) as usize) < MAX_CONNS {
            let stream = stream.unwrap();
            let conns = Arc::clone(&conns);
            conns.fetch_add(1, SeqCst);
            pool.spawn(|| handler_wrapper(stream, conns));
        } else {
            drop(stream);
        }
    }
}

fn handle_request_data(line: &str) -> Result<json::JsonValue> {
    let mut obj = json::object! {"method":"isPrime"};
    let mut err = false;
    eprintln!("{line}");
    match json::parse(line) {
        Ok(req) => match req {
            req if !req.has_key("number") || !req.has_key("method") => err = true,
            req if req["method"] != "isPrime" => err = true,
            _ => {
                let num = &req["number"];
                if num.contains(".") || num.contains("-") {
                    obj["prime"] = false.into();
                } else if let Some(v) = num.as_u64() {
                    obj["prime"] = is_prime(v).into();
                } else if num.as_number().is_some() {
                    obj["prime"] = false.into();
                } else {
                    err = true;
                }
            }
        },
        _ => err = true,
    }
    if err {
        Err(std::io::ErrorKind::InvalidData.into())
    } else {
        Ok(obj)
    }
}

fn handler_wrapper(stream: TcpStream, conns: Counter) {
    let _ = handle_connection(stream);
    conns.fetch_sub(1, SeqCst);
}

fn handle_connection(mut stream: TcpStream) -> Result<()> {
    let mut buffer = [0; 4068];
    stream.set_read_timeout(Some(Duration::new(5, 0)))?;

    'main: loop {
        let mut data = String::new();
        while let Ok(size) = stream.read(&mut buffer) {
            data.push_str(String::from_utf8_lossy(&buffer[0..size]).as_ref());

            if size == 0 || data.ends_with('\n') {
                break;
            }
        }

        if data.trim().is_empty() {
            stream.write_all("MALFORMED: Empty".as_bytes())?;
            break;
        }

        for line in data.lines() {
            match handle_request_data(line) {
                Ok(out) => {
                    let mut out = json::stringify(out).to_string();
                    out.push('\n');
                    stream.write_all(out.as_bytes())?;
                }
                Err(err) => {
                    stream.write_all(err.to_string().as_bytes())?;
                    break 'main;
                }
            }

            stream.flush()?;
        }
    }
    stream.shutdown(Shutdown::Read)?;
    Ok(())
}

fn is_prime(num: u64) -> bool {
    match num {
        0 | 1 => return false,
        2 | 3 => return true,
        n if n % 2 == 0 => return false,
        _ => {
            for n in (3..num)
                .filter(|n| n % 2 != 0)
                .take_while(|n| n.pow(2) <= num)
            {
                if num % n == 0 {
                    return false;
                }
            }
        }
    };
    true
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_prime() {
        assert!(is_prime(5));
        assert!(is_prime(7));
        assert!(!is_prime(9));
        assert!(is_prime(11));
        assert!(!is_prime(77));
        assert!(is_prime(87178291199));
    }
}
