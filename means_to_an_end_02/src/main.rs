use std::{
    collections::BTreeMap,
    io::{ErrorKind, Read, Result, Write},
    net::{Shutdown, TcpListener, TcpStream},
    sync::{atomic::AtomicU8, atomic::Ordering::SeqCst, Arc},
    thread,
    time::Duration,
};

use rayon::ThreadPoolBuilder;

type Counter = Arc<AtomicU8>;

const ADDR: (&str, u16) = ("0.0.0.0", 6969);
const NUM_THREADS: usize = 6;
const MAX_CONNS: usize = NUM_THREADS * 10;

#[derive(Debug, Copy, Clone)]
enum Msg {
    /// The min and max points of the inclusive time range
    Query(i32, i32),
    /// A time stamp and value
    Insert(i32, i32),
}

fn main() {
    let listener = TcpListener::bind(ADDR).unwrap();

    let pool = ThreadPoolBuilder::new()
        .num_threads(NUM_THREADS)
        .build()
        .unwrap();

    let conns = Arc::new(AtomicU8::new(0));

    for stream in listener.incoming() {
        if (conns.load(SeqCst) as usize) < MAX_CONNS {
            let stream = stream.unwrap();
            let conns = Arc::clone(&conns);
            conns.fetch_add(1, SeqCst);
            pool.spawn(|| handler_wrapper(stream, conns));
        } else {
            drop(stream);
        }
    }
}

fn handler_wrapper(stream: TcpStream, conns: Counter) {
    let _ = handle_connection(stream);
    conns.fetch_sub(1, SeqCst);
}

fn handle_connection(mut stream: TcpStream) -> Result<()> {
    stream.set_nodelay(true)?;
    stream.set_write_timeout(Some(Duration::from_secs(10)))?;
    stream.set_read_timeout(Some(Duration::from_secs(10)))?;

    let mut store: BTreeMap<i32, i32> = BTreeMap::new();

    let id = thread::current().id();

    let mut tmp = [0; 9];
    let mut cur = 0;
    'main: loop {
        let mut buf = [0; 4096];
        let mut msgs = Vec::new();
        'read: while let Ok(size) = stream.read(&mut buf) {
            if size == 0 {
                break;
            }
            for byt in buf[0..size].iter() {
                tmp[cur] = *byt;
                cur += 1;
                if cur == 9 {
                    cur = 0;
                    msgs.push(tmp);
                    if tmp[0] as char == 'Q' {
                        break 'read;
                    }
                }
            }
        }

        if msgs.is_empty() {
            break;
        }

        for msg in msgs {
            match decode(&msg) {
                Ok(out) => match out {
                    Msg::Query(min, max) => {
                        let avg = q_response(min, max, &store);
                        eprintln!("{id:?}: {out:?} => {avg}");
                        stream.write_all(&avg.to_be_bytes())?;
                        stream.flush()?;
                    }
                    Msg::Insert(t, v) => {
                        store.insert(t, v);
                    }
                },
                Err(err) => {
                    eprintln!("{id:?}: Err({err:?})");
                    stream.write_all(err.to_string().as_bytes())?;
                    stream.flush()?;
                    break 'main;
                }
            }
        }
    }
    stream.shutdown(Shutdown::Both)?;
    Ok(())
}

fn q_response(min: i32, max: i32, store: &BTreeMap<i32, i32>) -> i32 {
    let avg = if min <= max {
        let res = store.range(min..=max);
        let mut l = 0;
        let mut s = 0i64;
        for (i, e) in res.map(|e| e.1).enumerate() {
            s += *e as i64;
            l = i as i64 + 1;
        }
        if l > 0 {
            s / l
        } else {
            0
        }
    } else {
        0
    };
    avg as i32
}

fn decode(msg: &[u8]) -> Result<Msg> {
    let typ = msg[0] as char;
    let x = i32::from_be_bytes(msg[1..5].try_into().unwrap());
    let y = i32::from_be_bytes(msg[5..9].try_into().unwrap());
    match typ {
        'I' => Ok(Msg::Insert(x, y)),
        'Q' => Ok(Msg::Query(x, y)),
        _ => Err(ErrorKind::InvalidInput.into()),
    }
}
