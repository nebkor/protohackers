use std::{
    io::{prelude::*, BufReader},
    net::{TcpListener, TcpStream},
};

use rayon::ThreadPoolBuilder;

const ADDR: (&str, u16) = ("0.0.0.0", 6969);
const NUM_THREADS: usize = 10;

fn main() {
    let listener = TcpListener::bind(ADDR).unwrap();

    let pool = ThreadPoolBuilder::new()
        .num_threads(NUM_THREADS)
        .build()
        .unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        pool.spawn(|| handle_echo(stream));
    }
}

fn handle_echo(mut stream: TcpStream) {
    let sz = (&stream).bytes().size_hint().1.unwrap_or(512);
    let mut buf = Vec::with_capacity(sz);
    let mut buf_reader = BufReader::new(&mut stream);
    buf_reader.read_to_end(&mut buf).unwrap();
    stream.write_all(&buf).unwrap();
    stream.flush().unwrap();
    drop(stream);
}
